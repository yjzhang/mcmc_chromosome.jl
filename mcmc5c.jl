#implementation of mcmc5c?

using Distributions

# constant added to std deviation
# TODO: WHY
# If the initial data is already good, we can start with the std_const = 0.1
std_const = 1

# distance-to-contact-frequency function
f_dist(ds::Float64) = 1/ds

# pick a random point on a sphere of radius R
# returns x, y, z triple
function random_sphere(R::Float64)
    z = 4*rand()-2
    phi = 2*pi*rand()
    theta = asin(z/R)
    return [R*cos(theta)*cos(phi), R*cos(theta)*sin(phi), z]
end

# evaluate log likelihood of a conformation
# S is an N x 3 array (positions of each locus)
# D is an N x N array (inverse contact map from data) 
function log_likelihood(S::Array{Float64, 2}, D::Array{Float64, 2}; 
        std_const::Float64=1.0, use_hic::Bool=false)
    N = size(S)[1]
    ll = 0.0
    for i in 1:N
        for j in (i+1):N
            if (D[i,j] > 0)
                ds = sqrt((S[i,1]-S[j,1])^2+(S[i,2]-S[j,2])^2+(S[i,3]-S[j,3])^2)
                if use_hic
                    approx_reads = Normal(f_dist(ds), f_dist(ds)+std_const)
                else
                    approx_reads = Normal(f_dist(ds), std_const)
                end
                ll += logpdf(approx_reads, D[i,j]) 
            end
        end
    end
    return ll
end

function log_likelihood_poisson(S::Array{Float64, 2}, D::Array{Float64, 2}; 
        std_const::Float64=1.0)
    N = size(S)[1]
    ll = 0.0
    for i in 1:N
        for j in (i+1):N
            if (D[i,j] > 0)
                ds = sqrt((S[i,1]-S[j,1])^2+(S[i,2]-S[j,2])^2+(S[i,3]-S[j,3])^2)
                approx_reads = Poisson(f_dist(ds))
                ll += logpdf(approx_reads, D[i,j]) 
            end
        end
    end
    return ll
end

# If we already have a log-likelihood, we can update it with lower
# computational cost.
function ll_update(S::Array{Float64, 2}, D::Array{Float64, 2}, 
        update_index::Int, old_pos::Array{Float64, 1}, old_ll::Float64; 
        std_const::Float64=1.0, use_hic::Bool=false)
    # S is 3 x N (efficiency concerns
    N = size(D)[1]
    j = update_index
    ll = old_ll
    for i in 1:N
        if (i!=update_index && D[update_index, i]>0)
            ds = sqrt((old_pos[1]-S[1,i])^2+(old_pos[2]-S[2,i])^2+(old_pos[3]-S[3,i])^2)
            approx_reads = Normal(f_dist(ds), f_dist(ds)+std_const)
            ll -= logpdf(approx_reads, D[i,j])

            ds = sqrt((S[1,i]-S[1,j])^2+(S[2,i]-S[2,j])^2+(S[3,i]-S[3,j])^2)
            if use_hic
                approx_reads = Normal(f_dist(ds), f_dist(ds)+std_const)
            else
                approx_reads = Normal(f_dist(ds), std_const)
            end
            ll += logpdf(approx_reads, D[i,j]) 
        end
    end
    return ll
end

function ll_update_poisson(S::Array{Float64, 2}, D::Array{Float64, 2}, 
        update_index::Int, old_pos::Array{Float64, 2}, old_ll::Float64; 
        std_const::Float64=1.0)
    N = size(D)[1]
    j = update_index
    ll = old_ll
    for i in 1:N
        if (i!=update_index && D[update_index, i]>0)
            ds = sqrt((old_pos[1,1]-S[i,1])^2+(old_pos[1,2]-S[i,2])^2+(old_pos[1,3]-S[i,3])^2)
            approx_reads = Poisson(f_dist(ds))
            ll -= log(pdf(approx_reads, D[i,j]))

            ds = sqrt((S[i,1]-S[j,1])^2+(S[i,2]-S[j,2])^2+(S[i,3]-S[j,3])^2)
            approx_reads = Poisson(f_dist(ds))
            ll += log(pdf(approx_reads, D[i,j])) 
        end
    end
    return ll
end



# Random update for a single point
function random_update(R::Float64)
    point = [rand()*2*R-R, rand()*2*R-R, rand()*2*R-R]
    return point
end

# Runs MCMC5C for a given contact map. Returns the average output.
function run_mcmc(D::Array{Float64, 2}, n_iters::Int; rad = 1.0,
        update_rad::Float64 = 0.025,
        S::Array{Float64, 2} = zeros(1,3), std_const::Float64=1.0)
    N = size(D)[1]
    initial_x = [rand()*2*rad - rad for i in 1:3*N]
    if (S==zeros(1,3))
        S = reshape(initial_x, N, 3)
    end
    ll = log_likelihood(S, D, std_const=std_const)
    println(ll)
    #exit()
    #S_avg = zeros(size(S))
    S = S'
    for iter in 1:n_iters
        #S_avg += (1/n_iters)*S
        #println(ll)
        to_update = rand(1:N)
        old_position = S[:, to_update]
        update_distance = random_update(update_rad)
        S[:, to_update] += update_distance
        ll_new = ll_update(S, D, to_update, old_position, ll, 
            std_const=std_const)
        #ll_new = log_likelihood(S, D,std_const=std_const)
        if ll_new >= ll
            ll = ll_new
            #println("updated")
        else
            p = exp(ll_new - ll)
            if (rand() > p)
                S[:, to_update] = old_position
            else
                #println("updated_random")
                ll = ll_new
            end
        end
    end
    # TODO: return S or s_avg?
    return copy(S')
end
