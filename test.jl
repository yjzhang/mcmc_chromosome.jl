include("mcmc5c.jl")
include("../mds_julia/mds_approximate.jl")
include("../mds_julia/mds_metric.jl")
include("../mds_julia/output.jl")

#filename = "../test_data/combined_You_Pro_merged_chr18.csv"

filename = "data.txt"
filename_2 = "data_1.txt"

function run_mcmc(filename; output_suffix = "rand", mcmc_rounds=10)
    data = readcsv(filename, Float64)
    dist = load_data(filename, scale=1, exponent=1)
    data = remove_zeros(data)
    mds = MDSProblem(dist, 10.0)
    mds = remove_infs(mds)
    ipopt_problem = make_ipopt_problem(mds, radius_constraint=false,
        lad_constraint=false, nad_constraint=false)
    solveProblem(ipopt_problem)
    x = ipopt_problem.x
    coords = reshape(x, 3, int(length(x)/3))'
    output_pdb(coords, string("mds_", output_suffix, ".pdb"))
    output_txt(coords, string("mds_", output_suffix, ".txt"))

    dm = contact_freq_to_distance_matrix(filename, exponent=1, scale=1)
    x = mds_metric(dm)
    output_pdb(x, string("shortest_paths_", output_suffix, ".pdb"))
    output_txt(x, string("shortest_paths_", output_suffix, ".txt"))
    #println(size(data))
    #println(size(dist))
    #println(size(coords))
    S_avg = copy(coords)
    #S_avg = run_mcmc(data, 100000, S = copy(coords), std_const=1.0, rad=0.5)
    S = zeros(1,3)
    for i in 1:mcmc_rounds
        S_avg = run_mcmc(data, 100000, S = S, std_const=0.5, 
                rad=6.0, update_rad=0.015)
        S = S_avg
        output_txt(S_avg, string("mcmc_", string(i), output_suffix, ".txt"))
    end
    #output_pdb(x, "metric.pdb")
end

run_mcmc(filename_2, output_suffix = "3single", mcmc_rounds=100)
run_mcmc(filename_2, output_suffix = "4single", mcmc_rounds=100)
run_mcmc(filename, output_suffix = "3double", mcmc_rounds = 100)
run_mcmc(filename, output_suffix = "4double", mcmc_rounds = 100)


